<?php
session_start();
?>
<!DOCTYPE html >
<html>
<head>
    <!-- Basic Page Needs
================================================== -->
    <meta charset="utf-8">
    <!--    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">-->
    <link rel="icon" href="favicon.ico">
    <title>Maps - Brick it with ease</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="format-detection" content="telephone=no">
    <!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->


    <!-- Template CSS Files
    ================================================== -->
    <!-- Twitter Bootstrs CSS -->
    <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
    <!-- Ionicons Fonts Css -->
    <link rel="stylesheet" href="plugins/ionicons/ionicons.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="plugins/animate-css/animate.css">
    <!-- Hero area slider css-->
    <link rel="stylesheet" href="plugins/slider/slider.css">
    <!-- owl craousel css -->
    <link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="plugins/owl-carousel/owl.theme.css">
    <!-- Fancybox -->
    <link rel="stylesheet" href="plugins/facncybox/jquery.fancybox.css">
    <!-- template main css file -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <!--For search drop down -->
    <script type="text/javascript" src="https://unpkg.com/vue@latest"></script>
    <script type="text/javascript" src="https://unpkg.com/vue-simple-search-dropdown@latest/dist/vue-simple-search-dropdown.min.js"></script>
    <link rel="stylesheet" href="css/alertify.core.css">
    <link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS">
    <script type="text/javascript">
        Vue.use(Dropdown);
    </script>

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <title>Creating a Store Locator on Google Maps</title>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 500px;
            width: 90%;
            margin-left: 30px;
            position: absolute !important;

        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        .controls {
            margin-top: 10px;
            border: 1px solid transparent;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        }

        #origin-input,
        #destination-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 200px;
        }

        #origin-input:focus,
        #destination-input:focus {
            border-color: #4d90fe;
        }

        #mode-selector {
            color: #fff;
            background-color: #38923c;
            margin-left: 12px;
            padding: 5px 11px 0px 11px;
        }

        #mode-selector label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 400;
        }

    </style>
</head>

<body onload="initMap()">

<header id="top-bar" class="navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <!-- responsive nav button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- /responsive nav button -->
            <!-- logo -->
            <div class="navbar-brand">
                <a href="index.php">
                    <img src="images/downloaded/logo-onecolor-edited-white.png" alt="" class="project-logo">
                </a>
            </div>
            <!-- /logo -->
        </div>
        <!-- main menu -->
        <nav class="collapse navbar-collapse navbar-right" role="navigation">
            <div class="main-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php" style="font-weight: bold">Home</a>
                    </li>
                    <!--<li><`a href="about.html">About</a></li>-->
                    <li><a href="exercise.php" style="font-weight: bold">Exercises</a></li>
                    <li><a href="painandease.php" style="font-weight: bold">Pain and ease</a></li>
                    <li><a href="maps.php" style="font-weight: bold">Find places</a></li>
                    <li><a href="planner.php" style="font-weight: bold">Planner</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> My Account <span
                                    class="caret"></span></a>
                        <div class="dropdown-menu">
                            <ul>
                                <?php
                                if (isset($_SESSION['u_id'])) {
                                    echo '
                                        <li><a href="login.php">Dashboard</a></li>
                                        <li><form name="logout-form" action="includes/logout.inc.php" method="post">
                                    <li><a href="#" onclick="document.forms[0].submit()">Log out</a></li>
                                    </form> </li>';
                                } else {
                                    echo '
                                   
                                        <li><a href="login.php">Log in</a></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- /main nav -->
    </div>
</header>

<section class="global-page-header" style="background: #4b3434;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h2 id="heading">Places near you</h2>
                    <ol class="breadcrumb">
                        <li class="active" id="subheading" style="font-size: large"> Search for play areas near your favourite location in Victoria. Start searching now ! </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="wow fadeIn" style="margin-top: 20px;">
    <div class="row">
        <div class="col-md-12">
        <div class="col-xs-12 col-md-8">
                <row>
<!--                    <input id="origin-input" class="controls" type="text"-->
<!--                           placeholder="Enter an origin location">-->
<!---->
<!--                    <input id="destination-input" class="controls" type="text"-->
<!--                           placeholder="Enter a destination location">-->
<!---->
<!--                    <div id="mode-selector" class="controls">-->
<!--                        <input type="radio" name="type" id="changemode-walking" checked="checked">-->
<!--                        <label for="changemode-walking">Walking</label>-->
<!---->
<!--                        <input type="radio" name="type" id="changemode-transit">-->
<!--                        <label for="changemode-transit">Transit</label>-->
<!---->
<!--                        <input type="radio" name="type" id="changemode-driving">-->
<!--                        <label for="changemode-driving">Driving</label>-->
<!--                    </div>-->

                    <div id="map" class="wow fadeIn"></div>

                </row>
        </div>
        <div class="col-xs-12 col-md-4 wow fadeIn">
            <form style="width: 80%">
                <row>
                    <div class="form-group">
                        <label for="addressInput">Search Location</label><br><br>
                        <input type="text" class="form-control" id="addressInput" placeholder="Enter Address or Suburb">
                        <h6 style="font-family: Arial, sans-serif; font-size: 12px; padding-top: 5px;">Your current position will be considered if left blank.</h6>
                    </div>
                </row>
                <row>
<!--                    <div id="app" class="form-group">-->
<!--                        <label for="sportInput">Search Sports</label><br><br>-->
<!--                        <input type="text" class="form-control" id="sportInput" placeholder="Enter sport details here">-->
<!---->
<!--                    </div>-->
                    <div class="form-group">
                        <label for="sportInput">Select Sport</label><br><br>
                        <select class="form-control" id="sportInput" label="Sport">
                            <option value="">All</option>
                            <option value="Australian Rules Football">Australian Rules Football</option>
                            <option value="Aerobics">Aerobics</option>
                            <option value="Athletics">Athletics</option>
                            <option value="Badminton">Badminton</option>
                            <option value="Baseball">Baseball</option>
                            <option value="Basketball">Basketball</option>
                            <option value="BMX">BMX</option>
                            <option value="Beach Volleyball">Beach Volleyball</option>
                            <option value="Bocce">Bocce</option>
                            <option value="Body Building">Body Building</option>
                            <option value="Boxing">Boxing</option>
                            <option value="Callisthenics">Callisthenics</option>
                            <option value="Canoe Polo">Canoe Polo</option>
                            <option value="Canoeing">Canoeing</option>
                            <option value="Carpet Bowls">Carpet Bowls</option>
                            <option value="Cricket">Cricket</option>
                            <option value="Cricket (Indoor)">Cricket (Indoor)</option>
                            <option value="Cycling">Cycling</option>
                            <option value="Dancing">Dancing</option>
                            <option value="Disk Golf">Disk Golf</option>
                            <option value="Diving">Diving</option>
                            <option value="Fitness / Gymnasium Workouts">Fitness / Gymnasium Workouts</option>
                            <option value="Golf">Golf</option>
                            <option value="Gymnastics">Gymnastics</option>
                            <option value="Hockey">Hockey</option>
                            <option value="Ice Hockey">Ice Hockey</option>
                            <option value="Lacrosse">Lacrosse</option>
                            <option value="Martial Arts">Martial Arts</option>
                            <option value="Netball">Netball</option>
                            <option value="Netball (Indoor)">Netball (Indoor)</option>
                            <option value="Open Space">Open Space</option>
                            <option value="Rock Climbing / Abseiling (Indoor)">Rock Climbing / Abseiling (Indoor)</option>
                            <option value="Skating">Skating</option>
                            <option value="Soccer">Soccer</option>
                            <option value="Soccer (Indoor Soccer / Futsal)">Soccer (Indoor Soccer / Futsal)</option>
                            <option value="Softball">Softball</option>
                            <option value="Squash / Racquetball">Squash / Racquetball</option>
                            <option value="Swimming">Swimming</option>
                            <option value="Table Tennis">Table Tennis</option>
                            <option value="Team Handball">Team Handball</option>
                            <option value="Tennis (Indoor)">Tennis (Indoor)</option>
                            <option value="Tennis (Outdoor)">Tennis (Outdoor)</option>
                            <option value="Touch Football">Touch Football</option>
                            <option value="Underwater Hockey">Underwater Hockey</option>
                            <option value="Volleyball">Volleyball</option>
                            <option value="Water Polo">Water <Polo></Polo></option>
                            <option value="Wheelchair Sports">Wheelchair Sports</option>
                        </select>
                    </div>
                </row>
                <row>
                    <div class="form-group">
                        <label for="radiusSelect">Radius</label><br><br>
                        <select class="form-control" id="radiusSelect" label="Radius">
                            <option value="20">20 kms</option>
                            <option value="10">10 kms</option>
                            <option value="5" selected>5 kms</option>
                            <option value="1">1 kms</option>
                        </select>
                    </div>
                </row>
                <row>
                    <div class="form-group" style="text-align: center">
                        <button class="btn btn-dark-green" type="button" id="searchButton" value="Search">Search</button>
                    </div>
                </row>
                <row>
                    <div class="form-group">
                        <label for="results" id="results" style="visibility: hidden">Results</label><br><br>
                        <select label="results" class="form-control" id="locationSelect" style="visibility: hidden">Select one from all</select>
                    </div>
                </row>
            </form>
        </div>
    </div>
    </div>
</div>

<section id="call-to-action" style="margin-top: 150px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">Start exercising
                        today</h2>
                    <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">Take care of your body.
                        Plan with us to help ease your pain</p>
                </div>
            </div>
        </div><br>
        <a href="planner.php" class="btn btn-deep-orange btn-contact wow fadeInDown" data-wow-delay=".7s"
           data-wow-duration="500ms" style="font-family: Arial, sans-serif"><b>Personalised Planner</b></a>
    </div>
</section>

<!--    ==================================================
        Footer Section Start
        ================================================== -->
<footer id="footer">
    <div class="container">
        <div class="">
            <p class="copyright" style="text-align: center">&copy; Copyright: <span><script>document.write(new Date().getFullYear())</script></span>
                Design and Developed by <b>Team Pretty Pretty</b><br>
            </p>
        </div>
    </div>
    </div>
</footer> <!-- /#footer -->

<script src="js/maps.js">
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsnuXejoOhwXAMFjFROON3rUn4iFbch-g&libraries=places&callback=initMap">
</script>
<script src="plugins/jQurey/jquery.min.js"></script>
<!-- Form Validation -->
<script src="plugins/form-validation/jquery.form.js"></script>
<script src="plugins/form-validation/jquery.validate.min.js"></script>
<!-- owl carouserl js -->
<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
<!-- bootstrap js -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<!-- wow js -->
<script src="plugins/wow-js/wow.min.js"></script>
<!-- slider js -->
<script src="plugins/slider/slider.js"></script>
<!-- Fancybox -->
<script src="plugins/facncybox/jquery.fancybox.js"></script>
<!-- template main js -->
<script src="js/main.js"></script>
<script src="js/mdb.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpstJUGMxJfBPJ1Z3DWy8d5VPFpb57yDQ&libraries=places&callback=initMap" async defer></script>

<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsnuXejoOhwXAMFjFROON3rUn4iFbch-g&libraries=places&callback=initMap" async defer></script>-->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwZi05tN5K39Bp4xXbISybqPj8WeOnypc&libraries=places&callback=initAutocomplete"-->
<!--        async defer></script>-->
<script src="js/jquery-1.10.2.js"></script>
<script src="js/selectize.js"></script>
<script src="js/alertify.min.js"></script>

</body>
</html>