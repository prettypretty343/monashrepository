<!DOCTYPE html >
<html>
<head>
    <!-- Basic Page Needs
================================================== -->
    <meta charset="utf-8">
    <!--    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">-->
    <link rel="icon" href="favicon.ico">
    <title>Brick it with ease - Maps</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="format-detection" content="telephone=no">
    <!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->


    <!-- Template CSS Files
    ================================================== -->
    <!-- Twitter Bootstrs CSS -->
    <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
    <!-- Ionicons Fonts Css -->
    <link rel="stylesheet" href="plugins/ionicons/ionicons.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="plugins/animate-css/animate.css">
    <!-- Hero area slider css-->
    <link rel="stylesheet" href="plugins/slider/slider.css">
    <!-- owl craousel css -->
    <link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="plugins/owl-carousel/owl.theme.css">
    <!-- Fancybox -->
    <link rel="stylesheet" href="plugins/facncybox/jquery.fancybox.css">
    <!-- template main css file -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <!--For search drop down -->
    <script type="text/javascript" src="https://unpkg.com/vue@latest"></script>
    <script type="text/javascript" src="https://unpkg.com/vue-simple-search-dropdown@latest/dist/vue-simple-search-dropdown.min.js"></script>
    <script type="text/javascript">
        Vue.use(Dropdown);
    </script>

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <title>Creating a Store Locator on Google Maps</title>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 500px;
            width: 90%;
            margin-left: 30px;
            position: absolute !important;

        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>

<body onload="initMap()">

<header id="top-bar" class="navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <!-- responsive nav button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- /responsive nav button -->
            <!-- logo -->
            <div class="navbar-brand">
                <a href="index.html">
                    <img src="images/downloaded/logo-one-color-edited.png" alt="" class="project-logo">
                </a>
            </div>
            <!-- /logo -->
        </div>
        <!-- main menu -->
        <nav class="collapse navbar-collapse navbar-right" role="navigation">
            <div class="main-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.html" style="font-weight: bold">Home</a>
                    </li>
                    <!--<li><`a href="about.html">About</a></li>-->
                    <li><a href="exercise.php" style="font-weight: bold">Exercises</a></li>
                    <li><a href="painandease.php" style="font-weight: bold">Pain and ease</a></li>
                    <li><a href="maps.php" style="font-weight: bold">Find places</a></li>
                    <li><a href="login.php" style="font-weight: bold">Planner</a></li>

                </ul>
            </div>
        </nav>
        <!-- /main nav -->
    </div>
</header>

<section class="global-page-header" style="background: #0f4154">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block .animated .slideInLeft">
                    <h2 id="heading">Places near you</h2>
                    <ol class="breadcrumb">
                        <li class="active" id="subheading" style="font-size: large"> Search for play areas near your favourite location. Start searching now ! </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>

<!--<div style="margin-left: 30px;padding-top: 20px;" class="">-->
<!--    <form>-->
<!--        <row>-->
<!--            <div class="form-group col-md-4">-->
<!--                <label for="addressInput">Search Location</label><br><br>-->
<!--                <input type="text" class="form-control" id="addressInput" placeholder="Enter location details here">-->
<!--            </div>-->
<!--            <div class="form-group col-md-3">-->
<!--                <label for="radiusSelect">Radius</label><br><br>-->
<!--                <select class="form-control" id="radiusSelect" label="Radius">-->
<!--                    <option value="20">20 kms</option>-->
<!--                    <option value="10">10 kms</option>-->
<!--                    <option value="5" selected>5 kms</option>-->
<!--                    <option value="1">1 kms</option>-->
<!--                </select>-->
<!--            </div>-->
<!--            <div class="form-group col-md-3">-->
<!--                <input class="btn btn-green" type="button" id="searchButton" value="Search"/>-->
<!--            </div>-->
<!--        </row>-->
<!--        <row>-->
<!--            <div class="form-group col-md-6">-->
<!--                <select  class="form-control" id="locationSelect" style="width: 50%; visibility: hidden">Select one from all</select>-->
<!--            </div>-->
<!--        </row>-->
<!--        <row>-->
<!--            <div id="map" class="col-md-8"></div>-->
<!---->
<!--        </row>-->
<!--    </form>-->
<!--</div>-->
<!--<div id="map" style="width: 90%; height: 70%; margin-left: 30px;"></div>-->
<div class="" style="margin-top: 20px;">
    <div class="row">
        <div class="col-xs-12 col-md-8">

<!--                <row>-->
<!--                    <div class="form-group col-md-4">-->
<!--                        <label for="addressInput">Search Location</label><br><br>-->
<!--                        <input type="text" class="form-control" id="addressInput" placeholder="Enter location details here">-->
<!--                    </div>-->
<!--                    <div class="form-group col-md-3">-->
<!--                        <label for="radiusSelect">Radius</label><br><br>-->
<!--                        <select class="form-control" id="radiusSelect" label="Radius">-->
<!--                            <option value="20">20 kms</option>-->
<!--                            <option value="10">10 kms</option>-->
<!--                            <option value="5" selected>5 kms</option>-->
<!--                            <option value="1">1 kms</option>-->
<!--                        </select>-->
<!--                    </div>-->
<!--                    <div class="form-group col-md-3">-->
<!--                        <input class="btn btn-green" type="button" id="searchButton" value="Search"/>-->
<!--                    </div>-->
<!--                </row>-->
<!--                <row>-->
<!--                    <div class="form-group col-md-6">-->
<!--                        <select  class="form-control" id="locationSelect" style="width: 50%; visibility: hidden">Select one from all</select>-->
<!--                    </div>-->
<!--                </row>-->
                <row>
                    <div id="map" class=""></div>

                </row>
        </div>
        <div class="col-xs-12 col-md-4">
            <form style="width: 80%">
                <row>
                    <div class="form-group">
                        <label for="addressInput">Search Location</label><br><br>
                        <input type="text" class="form-control" id="addressInput" placeholder="Enter location details here">
                    </div>
                </row>
                <row>
                    <div id="app" class="form-group">
                        <label for="sportInput">Search Sports</label><br><br>
                        <input type="text" class="form-control" id="sportInput" placeholder="Enter sport details here">
<!--                        <Dropdown-->
<!--                                :options="options"-->
<!--                                v-on:selected="validateSelection"-->
<!--                                v-on:filter="getDropdownValues"-->
<!--                                :disabled="false"-->
<!--                                placeholder="Please select a sport"-->
<!--                                id="sportInput"-->
<!--                                v-->
<!--                                >-->
<!--                        </Dropdown>-->
<!--                        <p>Selected animal: {{ selected.name || 'none' }}</p>-->

                    </div>
                </row>
                <row>
                    <div class="form-group">
                        <label for="radiusSelect">Radius</label><br><br>
                        <select class="form-control" id="radiusSelect" label="Radius">
                            <option value="20">20 kms</option>
                            <option value="10">10 kms</option>
                            <option value="5" selected>5 kms</option>
                            <option value="1">1 kms</option>
                        </select>
                    </div>
                </row>
                <row>
                    <div class="form-group">
                        <input class="btn btn-green" type="button" id="searchButton" value="Search"/>
                    </div>
                </row>
                <row>
                    <div class="form-group">
                        <label for="results" id="results" style="visibility: hidden">Results</label><br><br>
                        <select label="results" class="form-control" id="locationSelect" style="visibility: hidden">Select one from all</select>
                    </div>
                </row>
            </form>
        </div>
    </div>
</div>

<section id="call-to-action" style="margin-top: 250px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">Start exercising
                        today</h2>
                    <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">Take care of your body.
                        Plan with us to help ease your pain</p>
                </div>
            </div>
        </div><br>
        <a href="login.php" class="btn btn-primary btn-contact wow fadeInDown" data-wow-delay=".7s"
           data-wow-duration="500ms" style="font-family: Arial, sans-serif"><b>Personalised Planner</b></a>
    </div>
</section>

<!--    ==================================================
        Footer Section Start
        ================================================== -->
<footer id="footer">
    <div class="container">
        <div class="col-md-8">
            <p class="copyright">Copyright: <span><script>document.write(new Date().getFullYear())</script></span>
                Design and Developed by <a href="https://www.Themefisher.com" target="_blank">Themefisher</a>. <br>
            </p>
        </div>
    </div>
    </div>
</footer> <!-- /#footer -->

<script src="js/maps.js">
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsnuXejoOhwXAMFjFROON3rUn4iFbch-g&libraries=places&callback=initMap">
</script>
<script src="plugins/jQurey/jquery.min.js"></script>
<!-- Form Validation -->
<script src="plugins/form-validation/jquery.form.js"></script>
<script src="plugins/form-validation/jquery.validate.min.js"></script>
<!-- owl carouserl js -->
<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
<!-- bootstrap js -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<!-- wow js -->
<script src="plugins/wow-js/wow.min.js"></script>
<!-- slider js -->
<script src="plugins/slider/slider.js"></script>
<!-- Fancybox -->
<script src="plugins/facncybox/jquery.fancybox.js"></script>
<!-- template main js -->
<script src="js/main.js"></script>
<script src="js/mdb.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsnuXejoOhwXAMFjFROON3rUn4iFbch-g&libraries=places&sensor=false"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwZi05tN5K39Bp4xXbISybqPj8WeOnypc&libraries=places&callback=initAutocomplete"-->
<!--        async defer></script>-->
<script src="js/jquery-1.10.2.js"></script>
<script src="js/selectize.js"></script>


<!--<script type="text/javascript">-->
<!--    Vue.use(Dropdown);-->
<!--    new Vue({-->
<!--        el: '#app',-->
<!--        data () {-->
<!--            return {-->
<!--                options: [-->
<!--                    { name: 'Australian Rules Football', id: 'Australian Rules Football' },-->
<!--                    { name: 'Cricket', id: 'Cricket' },-->
<!--                    { name: 'Netball', id: 'Netball' },-->
<!--                    { name: 'Tennis', id: 'Tennis' },-->
<!--                    { name: 'Aerobics', id: 'Aerobics' },-->
<!--                    { name: 'Athletics', id: 'Athletics' },-->
<!--                    { name: 'Badminton', id: 'Badminton' }-->
<!--                ],-->
<!--                selected: { name: null, id: null }-->
<!--            }-->
<!--        },-->
<!--        methods: {-->
<!--            validateSelection(selection) {-->
<!--                this.selected = selection;-->
<!--                console.log(selection.name+' has been selected');-->
<!--            },-->
<!--            getDropdownValues(keyword) {-->
<!--                console.log('You could refresh options by querying the API with '+keyword);-->
<!--            }-->
<!--        }-->
<!--    });-->
<!---->
<!---->
<!--</script>-->
<script>
    $(function() {
        $('#select-city').selectize(options);
    });
</script>

</body>
</html>