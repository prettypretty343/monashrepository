<?php
session_start();
?>
<html>
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="icon" href="favicon.ico">
    <title>Home - Brick it with ease</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Template CSS Files
    ================================================== -->
    <!-- Twitter Bootstrs CSS -->
    <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
    <!-- Ionicons Fonts Css -->
    <link rel="stylesheet" href="plugins/ionicons/ionicons.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="plugins/animate-css/animate.css">
    <!-- Hero area slider css-->
    <link rel="stylesheet" href="plugins/slider/slider.css">
    <!-- owl craousel css -->
    <link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="plugins/owl-carousel/owl.theme.css">
    <!-- Fancybox -->
    <link rel="stylesheet" href="plugins/facncybox/jquery.fancybox.css">
    <!-- template main css file -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>

</head>
<body>
<button id="myBtn" title="Go to top" data-wow-delay=".5s"
        class="wow fadeInUp animated smooth-scroll" href="#hero-area" data-section="#hero-area">Top
</button>
<!--
==================================================
Header Section Start
================================================== -->
<header id="top-bar" class="navbar-fixed-top animated-header">
    <div class="container">
        <div class="navbar-header">
            <!-- responsive nav button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- /responsive nav button -->
            <!-- logo -->
            <div class="navbar-brand">
                <a href="index.php">
                    <img src="images/downloaded/logo-onecolor-edited-white.png" alt="" class="project-logo">
                </a>
            </div>
            <!-- /logo -->
        </div>
        <!-- main menu -->
        <nav class="collapse navbar-collapse navbar-right" role="navigation">
            <div class="main-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php" style="font-weight: bold">Home</a>
                    </li>
                    <!--<li><`a href="about.html">About</a></li>-->
                    <li><a href="exercise.php" style="font-weight: bold">Exercises</a></li>
                    <li><a href="painandease.php" style="font-weight: bold">Pain and ease</a></li>
                    <li><a href="maps.php" style="font-weight: bold">Find places</a></li>
                    <li><a href="planner.php" style="font-weight: bold">Planner</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> My Account <span
                                    class="caret"></span></a>
                        <div class="dropdown-menu">
                            <ul>
                                <?php
                                if (isset($_SESSION['u_id'])) {
                                    echo '
                                        <li><a href="login.php">Dashboard</a></li>
                                        <li><form name="logout-form" action="includes/logout.inc.php" method="post">
                                    <li><a href="#" onclick="document.forms[0].submit()">Log out</a></li>
                                    </form> </li>';
                                } else {
                                    echo '
                                   
                                        <li><a href="login.php">Log in</a></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- /main nav -->
    </div>
</header>


<!--
==================================================
Slider Section Start
================================================== -->

<section id="hero-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="block wow fadeInUp" data-wow-delay=".3s">
                    <!-- Slider -->
                    <section class="cd-intro">
                        <h1 class="wow fadeInUp animated cd-headline slide" data-wow-delay=".4s">
                            <span>HI, MY NAME IS TIM & I AM A</span><br>
                            <span class="cd-words-wrapper">
                                <b class="is-visible" style="">BRICKY</b>
                                <!--<b>DEVELOPER</b>
                                <b>FATHER</b>-->
                            </span>
                        </h1>
                    </section> <!-- cd-intro -->
                    <!-- /.slider -->
                    <h2 class="wow fadeInUp animated" data-wow-delay=".6s"><b>
                            Brick it with ease brings in personalised planner and exercises for you.<br> They have great ways to ease pain of brickies suffering from Arthritis and to work with ease</b>
                    </h2>
                    <a class="btn-lines dark light wow fadeInUp animated smooth-scroll" data-wow-delay=".5s"
                       href="#about" data-section="#about">
                        <img src="images/arrow.png" class="arrow-size">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>


<!--<section id="hero-area">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-md-4">-->
<!--                <h2 class="wow fadeIn animated" data-wow-delay=".6s"><b>-->
<!--                        Brick it with ease brings in personalised planner and exercises for the brickies.<br><br> Brickies are suffering with arthritis and here you can find great ways to ease the pain and ways to work with ease </b>-->
<!--                </h2><br>-->
<!--            </div>-->

<!--                                <div class="col-md-12">-->
<!--                                    <div class="block wow fadeInDown" data-wow-delay=".3s">-->
<!---->
<!--                                        <div id="wowslider-container1">-->
<!--                                            <div class="ws_images"><ul>-->
<!--                                                    <li><img src="data1/images/slide1.jpg" alt="Slide1" title="Slide1" id="wows1_0"/></li>-->
<!--                                                    <li><img src="data1/images/slide5.jpg" alt="Slide5" title="Slide5" id="wows1_1"/></li>-->
<!--                                                    <li><img src="data1/images/slide4.jpg" alt="Slide4" title="Slide4" id="wows1_2"/></li>-->
<!--                                                    <li><a href="http://wowslider.net"><img src="data1/images/slide3.jpg" alt="bootstrap carousel" title="Slide3" id="wows1_3"/></a></li>-->
<!--                                                    <li><img src="data1/images/slide2.jpg" alt="Slide2" title="Slide2" id="wows1_4"/></li>-->
<!--                                                </ul></div>-->
<!--                                            <div class="ws_bullets"><div>-->
<!--                                                    <a href="#" title="Slide1"><span>1</span></a>-->
<!--                                                    <a href="#" title="Slide5"><span>2</span></a>-->
<!--                                                    <a href="#" title="Slide4"><span>3</span></a>-->
<!--                                                    <a href="#" title="Slide3"><span>4</span></a>-->
<!--                                                    <a href="#" title="Slide2"><span>5</span></a>-->
<!--                                                </div></div><div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.net">html slider</a> by WOWSlider.com v8.8</div>-->
<!--                                            <div class="ws_shadow"></div>-->
<!--                                        </div>-->


                    <!--                                        <section class="cd-intro">-->
<!--                        <h1 class="wow fadeInUp animated cd-headline slide" data-wow-delay=".4s">-->
<!--                            <span>HI, MY NAME IS TIM & I AM A</span><br>-->
<!--                            <span class="cd-words-wrapper">-->
<!--                                <b class="is-visible" style="">BRICKY</b>-->
<!--                                <!--<b>DEVELOPER</b>-->
<!--                                <b>FATHER</b>-->
<!--                            </span>-->
<!--                        </h1>-->
<!--                    </section> <!-- cd-intro -->
<!--                    <!-- /.slider -->
<!--                    <h2 class="wow fadeInUp animated" data-wow-delay=".6s"><b>-->
<!--                            Brick it with ease brings in personalised planner and exercises for you.<br> They have great ways to ease pain of brickies suffering from Arthritis and to work with ease</b>-->
<!--                    </h2>-->
<!--                    <a class="btn-lines dark light wow fadeInUp animated smooth-scroll" data-wow-delay=".5s"-->
<!--                       href="#about" data-section="#about">-->
<!--                        <img src="images/arrow.png" class="arrow-size">-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section><!--/#main-slider-->

<!--
==================================================
About Section Start
================================================== -->
<section id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="block wow fadeInLeft" data-wow-delay=".3s" data-wow-duration="500ms">
                    <h2>
                        What is Arthritis ?
                    </h2>
                    <p>
                        <b style="font-weight: bold"></b>Arthritis is very common but is not well understood.
                        Actually, “arthritis” is not a single disease; it is an informal way of referring to joint pain
                        or joint disease.People of all ages, sexes and races can and do have arthritis, and it is the
                        leading cause of disability in Australia. <br>More than 50 million adults and 300,000 children have some type
                        of arthritis. It is most common among women and occurs more frequently as people get older.<br>
                        Common arthritis joint symptoms include swelling, pain, stiffness and decreased range of motion.

                    </p>
                    <p>
                        'Brick it with ease' is here to improve quality of life by trying to ease the pain of millions
                        of Australians including the Brickworkers who
                        are most affected by condition of Arthritis.
                    </p>
                </div>

            </div>
            <div class="col-md-6 col-sm-6">
                <div class="block wow fadeInRight" data-wow-delay=".3s" data-wow-duration="500ms">
                    <img src="images/downloaded/arthritis-1.jpeg" alt="" class="image-radius image-height img-responsive">
                </div>
            </div>
            <!--<a class="btn-lines dark light wow fadeInUp animated smooth-scroll btn btn-default btn-green" data-wow-delay=".5s" href="#works" data-section="#works">-->
            <!--<img src = "images/arrow.png" class="arrow-size" width="40px" height="40px">-->
            <!--</a>-->
        </div>
    </div>
</section> <!-- /#about -->


<!--
==================================================
Portfolio Section Start
================================================== -->
<section id="works" class="works">
    <div class="container">
        <div class="section-heading">
            <h1 class="title wow fadeInDown" data-wow-delay=".3s">Common Symptoms</h1>
            <p class="wow fadeInDown" data-wow-delay=".5s">
                These are the common affected areas for all Brickworkers working in this industry
            </p>
        </div>
        <div class="row">
            <div class="col-sm-4 col-xs-12">
                <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                    <div class="img-wrapper">
                        <img src="images/downloaded/shoulder.jpg" class="symptom-images img-responsive"
                             alt="this is a title">
                        <div class="overlay">
                            <div class="buttons">
                                <a rel="gallery" class="fancybox" href="images/downloaded/shoulder.jpg">View</a>
                                <a href="details1.html">Details</a>
                            </div>
                        </div>
                    </div>
                    <figcaption>
                        <h4>
                            <a href="details1.html"><b>
                                    Shoulder pain</b>
                            </a>
                        </h4>
                        <p>

                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-sm-4 col-xs-12">
                <figure class="wow fadeInLeft animated" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="img-wrapper">
                        <img src="images/downloaded/backpain.jpg" class="symptom-images img-responsive"
                             alt="this is a title">
                        <div class="overlay">
                            <div class="buttons">
                                <a rel="gallery" class="fancybox" href="images/downloaded/backpain.jpg">View</a>
                                <a href="details2.html">Details</a>
                            </div>
                        </div>
                    </div>
                    <figcaption>
                        <h4>
                            <a href="details2.html">
                                <b>Back Pain</b>
                            </a>
                        </h4>
                        <p>

                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-sm-4 col-xs-12">
                <figure class="wow fadeInLeft animated" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="img-wrapper">
                        <img src="images/downloaded/wrist.jpg" class="symptom-images img-responsive" alt=""
                             width="100%">
                        <div class="overlay">
                            <div class="buttons">
                                <a rel="gallery" class="fancybox" href="images/downloaded/wrist.jpg">View</a>
                                <a href="details3.html">Details</a>
                            </div>
                        </div>
                    </div>
                    <figcaption>
                        <h4>
                            <a href="details3.html">
                                <b>Wrist pain</b>
                            </a>
                        </h4>
                        <p>

                        </p>
                    </figcaption>
                </figure>
            </div>
        </div>
        <!--<a class="btn-lines dark light wow fadeInUp animated smooth-scroll btn btn-default btn-green"-->
        <!--data-wow-delay=".5s" href="#feature" data-section="#feature">-->
        <!--<img src="images/arrow.png" class="arrow-size" width="40px" height="40px">-->
        <!--</a>-->
    </div>
</section> <!-- #works -->
<!--
==================================================
Portfolio Section Start
================================================== -->
<section id="feature" class="feature">
    <div class="container">
        <div class="section-heading">
            <h1 class="title wow fadeInDown" data-wow-delay=".3s">What we offer</h1>
            <p class="wow fadeInDown" data-wow-delay=".5s">
                Different Activities and fitness to ease your pain and stay healthy<br>
            </p>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="media wow fadeInUp animated" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="media-left">
                        <div class="icon">
                            <i class="ion-ios-body"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Suggested Physical activities</h4>
                        <p>From the experts to help you get the best to release your pain</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="600ms">
                    <div class="media-left">
                        <div class="icon">
                            <i class="ion-ios-lightbulb-outline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Interactive AV </h4>
                        <p>To enhance the experience of performing physical activities</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="900ms">
                    <div class="media-left">
                        <div class="icon">
                            <i class="ion-ios-book-outline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Personalised Planner</h4>
                        <p>Daily activity tracking to help improve health</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="1200ms">
                    <div class="media-left">
                        <div class="icon">
                            <i class="ion-ios-americanfootball-outline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Favourite sport activity information</h4>
                        <p>Suggesting preferred sport activities and their location neatby you</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="1500ms">
                    <div class="media-left">
                        <div class="icon">
                            <i class="ion-ios-keypad-outline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Expert Consultation</h4>
                        <p>Providing you with the best physiotherapists around you for consultation</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="media wow fadeInDown animated" data-wow-duration="500ms" data-wow-delay="1800ms">
                    <div class="media-left">
                        <div class="icon">
                            <i class="ion-ios-help-outline"></i>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Quick help</h4>
                        <p>Access to the FAQ's to help with some common questions</p>
                    </div>
                </div>
            </div>
        </div>
        <!--For implementing the down arrow on the screen-->
        <!--<a class="btn-lines dark light wow fadeInUp animated smooth-scroll btn btn-default btn-green" data-wow-delay=".5s" href="#call-to-action" data-section="#call-to-action">-->
        <!--<img src = "images/arrow.png" class="arrow-size" width="40px" height="40px">-->
        <!--</a>-->
    </div>
</section> <!-- /#feature -->


<!--
==================================================
Call To Action Section Start
================================================== -->

<div id="nav-footer"></div>
<script>
    $(function(){
        $("#nav-footer").load("footer-navbar.html");
    });
</script>

<!--
==================================================
Footer Section Start
================================================== -->

<footer id="footer">
    <div class="container">
        <div class="">
            <p class="copyright" style="text-align: center">&copy; Copyright: <span><script>document.write(new Date().getFullYear())</script></span>
                Design and Developed by <b>Team Pretty Pretty</b><br>
            </p>
        </div>
    </div>
    </div>
</footer>

<!--<footer id="footer">-->
<!--<div class="container">-->
<!--<div class="col-md-8">-->
<!--<p class="copyright">Copyright: <span><script>document.write(new Date().getFullYear())</script></span>-->
<!--Design and Developed by <a href="https://www.Themefisher.com" target="_blank">Themefisher</a>. <br>-->
<!--</p>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</footer> &lt;!&ndash; /#footer &ndash;&gt;-->

<!-- Template Javascript Files
================================================== -->
<!-- jquery -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="plugins/jQurey/jquery.min.js"></script>
<!-- Form Validation -->
<script src="plugins/form-validation/jquery.form.js"></script>
<script src="plugins/form-validation/jquery.validate.min.js"></script>
<!-- owl carouserl js -->
<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
<!-- bootstrap js -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<!-- wow js -->
<script src="plugins/wow-js/wow.min.js"></script>
<!-- slider js -->
<script src="plugins/slider/slider.js"></script>
<!-- Fancybox -->
<script src="plugins/facncybox/jquery.fancybox.js"></script>
<!-- template main js -->
<script src="js/main.js"></script>
<script type="text/javascript" src="engine1/wowslider.js"></script>
<script type="text/javascript" src="engine1/script.js"></script>
</body>
</html>