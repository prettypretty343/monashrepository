<!DOCTYPE html>
<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" href="favicon.ico">
        <title>Details - Brick it with ease</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">



        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="plugins/bootstrap/bootstrap.min.css">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="plugins/ionicons/ionicons.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="plugins/animate-css/animate.css">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="plugins/slider/slider.css">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="plugins/owl-carousel/owl.theme.css">
        <!-- Fancybox -->
        <link rel="stylesheet" href="plugins/facncybox/jquery.fancybox.css">
        <!-- template main css file -->
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>


        <!--
        ==================================================
        Header Section Start
        ================================================== -->
        <header id="top-bar" class="navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- /responsive nav button -->

                    <!-- logo -->
                    <div class="navbar-brand">
                        <a href="index.html">
                            <img src="images/downloaded/logo-project-edited.png" alt="" class="project-logo">
                        </a>
                    </div>
                    <!-- /logo -->
                </div>
                <!-- main menu -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <div class="main-menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <!--<li><a href="about.html">About</a></li>-->
                            <li><a href="service.php">Exercises</a></li>
                            <li><a href="blog-fullwidth.html">Pain and ease</a></li>
                            <!--<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
                                <div class="dropdown-menu">
                                    <ul>
                                        <li><a href="404.html">404 Page</a></li>
                                        <li><a href="gallery.html">Gallery</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog <span class="caret"></span></a>
                                <div class="dropdown-menu">
                                    <ul>
                                        <li><a href="blog-fullwidth.html">Blog Full</a></li>
                                        <li><a href="blog-left-sidebar.html">Blog Left sidebar</a></li>
                                        <li><a href="blog-right-sidebar.html">Blog Right sidebar</a></li>
                                    </ul>
                                </div>
                            </li>-->
                            <li><a href="contact.html">Find places</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- /main nav -->
            </div>
        </header>



        <!--
        ==================================================
        Global Page Section Start
        ================================================== -->
<section class="global-page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h2>Shoulder Arthritis</h2>
                    <div class="portfolio-meta">
                       <span></span>
                       <span></span>
                       <!-- <span>----<a href="http://www.themefisher.com">Themefisher</a></span> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!--/#Page header-->

<section class="portfolio-single">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="portfolio-single-img col-md-6">
                    <img class="detail-image" alt="" src="images/downloaded/shoulder.jpg">
                </div>
                <div class="col-md-6">
                    <h3>Rheumatoid Arthritis</h3>
                    <h3>Osteoarthritis</h3>
                    <h3>Rotator Cuff Tear Arthropathy</h3>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <div class="portfolio-single-img col-md-12">
                </div>
                <div class="col-md-12">
                    <h3>Rheumatoid Arthritis</h3>
                    <p>
                        Rheumatoid Arthritis
                        One common form of shoulder arthritis is an autoimmune condition called rheumatoid arthritis (RA). You may have pain in both shoulders at once if you have RA. You might also experience: tenderness and warmth in your joints
                        a stiff feeling in your shoulders, especially in the morning
                        rheumatoid nodules, which are bumps under your skin in your shoulders or arms
                        fatigue, weight loss, or fever
                        RA affects your joint lining and can cause joint swelling as well. It can cause erosion of your shoulder bones and deformity of your shoulder joints over time.
                    </p>
                    <h3>Osteoarthritis</h3>
                    <p>
                        Also known as "wear-and-tear" arthritis, osteoarthritis is a condition that destroys the smooth outer covering (articular cartilage) of bone. As the cartilage wears away, it becomes frayed and rough, and the protective space between the bones decreases. During movement, the bones of the joint rub against each other, causing pain. Osteoarthritis usually affects people over 50 years of age and is more common in the acromioclavicular joint than in the glenohumeral shoulder joint.
                    </p>

                    <h3>Rotator Cuff Tear Arthropathy</h3>
                    <p>
                        Your shoulder contains a rotator cuff, which connects the shoulder blade with the top of your arm through a collection of tendons and muscles. Injuries to the rotator cuff are common and can lead to a form of shoulder arthritis called rotator cuff tear arthropathy.
                        A rip in the tendons of the rotator cuff is generally what causes this condition. Arthritis develops in your shoulder when bones in the shoulder are damaged. Symptoms include intense pain and muscle weakness that can make lifting overhead difficult
                    </p>

                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <div class="portfolio-single-img col-md-6">
                </div>
                <div class="col-md-6">

              
                </div>
            </div>
        </div>
    </div>
</section>

<section class="works works-fit">
    <div class="container">
        <h2 class="subtitle">Some Of other forms of Arhtritis</h2>
            <p class="subtitle-des">
                Other than Shoulder Arhtritis, you can check the following
            </p>
        <div class="row">
            <div class="col-sm-3">
                <figure class="wow fadeInLeft animated" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="img-wrapper">
                        <img src="images/downloaded/backpain.jpg" class="symptom-images img-responsive" alt="this is a title" >
                        <div class="overlay">
                            <div class="buttons">
                                <a rel="gallery" class="fancybox" href="images/downloaded/backpain.jpg">View</a>
                                <a href="details2.html">Details</a>
                            </div>
                        </div>
                    </div>
                    <figcaption>
                    <h4>
                    <a href="details2.html">
                        Back Pain
                    </a>
                    </h4>
                    <p>
                        
                    </p>
                    </figcaption>
                </figure>
            </div>

            <div class="col-sm-3">
                <figure class="wow fadeInLeft animated" data-wow-duration="500ms" data-wow-delay="300ms">
                    <div class="img-wrapper">
                        <img src="images/downloaded/wrist.jpg" class="symptom-images img-responsive" alt="" width="100%">
                        <div class="overlay">
                            <div class="buttons">
                                <a rel="gallery" class="fancybox" href="images/downloaded/wrist.jpg">View</a>
                                <a href="details3.html">Details</a>
                            </div>
                        </div>
                    </div>
                    <figcaption>
                    <h4>
                    <a href="details3.html">
                        Wrist pain
                    </a>
                    </h4>
                    <p>
                        
                    </p>
                    </figcaption>
                </figure>
            </div>

            <!-- <div class="col-sm-3">
                <figure class="wow fadeInLeft animated animated" data-wow-duration="300ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 300ms; -webkit-animation-duration: 300ms; animation-delay: 300ms; -webkit-animation-delay: 300ms; animation-name: fadeInLeft; -webkit-animation-name: fadeInLeft;">
                    <div class="img-wrapper">
                        <img src="images/portfolio/item-3.jpg" class="img-responsive" alt="">
                        <div class="overlay">
                            <div class="buttons">
                                <a rel="gallery" title="Proin imperdiet augue et magna interdum hendrerit" class="fancybox" href="images/portfolio/item-3.jpg">Demo</a>        
                                <a target="_blank" href="single-postfolio.html">Details</a>
                            </div>
                        </div>
                    </div>
                    <figcaption>
                        <h4>
                            <a href="#">
                                Table Design        
                            </a>
                        </h4>
                        <p>
                            Lorem ipsum dolor sit amet.
                        </p>
                    </figcaption>
                </figure>
            </div>

            <div class="col-sm-3">
                <figure class="wow fadeInLeft animated animated" data-wow-duration="300ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 300ms; -webkit-animation-duration: 300ms; animation-delay: 600ms; -webkit-animation-delay: 600ms; animation-name: fadeInLeft; -webkit-animation-name: fadeInLeft;">
                    <div class="img-wrapper">
                        <img src="images/portfolio/item-4.jpg" class="img-responsive" alt="">
                        <div class="overlay">
                            <div class="buttons">
                                <a rel="gallery" title="Proin imperdiet augue et magna interdum hendrerit" class="fancybox" href="images/portfolio/item-4.jpg">Demo</a>        
                                <a target="_blank" href="single-postfolio.html">Details</a>
                            </div>
                        </div>
                    </div>
                    <figcaption>
                        <h4>
                            <a href="#">
                                Make Up elements        
                            </a>
                        </h4>
                        <p>
                            Lorem ipsum dolor.
                        </p>
                    </figcaption>
                </figure>
            </div> -->
        </div>
    </div>
</section>   



        
            <!--
            ==================================================
            Call To Action Section Start
            ================================================== -->
            <section id="call-to-action">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <h2 class="title wow fadeInDown" data-wow-delay=".3s" data-wow-duration="500ms">SO WHAT YOU THINK ?</h2>
                                <p class="wow fadeInDown" data-wow-delay=".5s" data-wow-duration="500ms">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis,<br>possimus commodi, fugiat magnam temporibus vero magni recusandae? Dolore, maxime praesentium.</p>
                                <a href="contact.html" class="btn btn-default btn-contact wow fadeInDown" data-wow-delay=".7s" data-wow-duration="500ms">Contact With Me</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </section>;

            <!--
            ==================================================
            Footer Section Start
            ================================================== -->
            <footer id="footer">
                <div class="container">
                    <div class="col-md-8">
                        <p class="copyright">Copyright: <span><script>document.write(new Date().getFullYear())</script></span> Design and Developed by <a href="https://www.Themefisher.com" target="_blank">Themefisher</a>. <br>
                            Get More 
                            <a href="https://themefisher.com/free-bootstrap-templates/" target="_blank">
                                Free Bootstrap Templates
                            </a>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <!-- Social Media -->
                        <ul class="social">
                            <li>
                                <a href="http://wwww.fb.com/themefisher" class="Facebook">
                                    <i class="ion-social-facebook"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer> <!-- /#footer -->

	<!-- Template Javascript Files
	================================================== -->
	<!-- jquery -->
	<script src="plugins/jQurey/jquery.min.js"></script>
	<!-- Form Validation -->
    <script src="plugins/form-validation/jquery.form.js"></script> 
    <script src="plugins/form-validation/jquery.validate.min.js"></script>
	<!-- owl carouserl js -->
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<!-- bootstrap js -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<!-- wow js -->
	<script src="plugins/wow-js/wow.min.js"></script>
	<!-- slider js -->
	<script src="plugins/slider/slider.js"></script>
	<!-- Fancybox -->
	<script src="plugins/facncybox/jquery.fancybox.js"></script>
	<!-- template main js -->
	<script src="js/main.js"></script>
 	</body>
</html>